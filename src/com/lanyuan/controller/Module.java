package com.lanyuan.controller;

import java.util.List;

/**
 * 
 * @author HuangWenJing
 * 
 * 模块传输对象
 */
public class Module {

	private List<String> pmodules;
	private List<String> modules;

	public List<String> getPmodules() {
		return pmodules;
	}

	public void setPmodules(List<String> pmodules) {
		this.pmodules = pmodules;
	}

	public List<String> getModules() {
		return modules;
	}

	public void setModules(List<String> modules) {
		this.modules = modules;
	}

}
